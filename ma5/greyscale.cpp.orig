#include "greyscale.h"
#include <iomanip> //nur für Testzwecke

int GreyScale::format=0;

string GreyScale::readNext(istream& istr) {
    istr>>ws;
    string currentString="";
    char currentChar=istr.get();
    if (currentChar=='#') { //Ignoriere Kommentare
        while (!istr.fail()&&!istr.eof()&&currentChar!=10/*Line Feed*/) {
            //currentString+=currentChar;
            currentChar=istr.get();
        }
        istr>>ws;
        currentChar=istr.get();
    }
    while (!isspace(currentChar)&&!istr.fail()&&!istr.eof()) {
        currentString+=currentChar;
        currentChar=istr.get();
    }
    return currentString;
}

GreyScale GreyScale::wende_an(char filter) const {
    GreyScale result(width, height, depth);
    for (int x=0, y=0; x<width; x++) {
        for (y=0; y<height; y++) {
            switch(filter) {
            case 'B' :
                result(x,y)= Blur_(x,y) ;
                break;
            case 'l' :
                result(x,y)= Laplace_(x,y);
                break;
            case 'k' :
                result(x,y)= Kirsch_(x,y);
                break;
            case 's' :
                result(x,y)= Sobel_(x,y);
                break;
            case 'c' :
                result(x,y)= Clamp_(x,y);
                break;
            case 'm' :
                result(x,y)= Median_(x,y);
                break;
            }
        }
    }
    return result;
}

float GreyScale::faltung(vector<vector<float>>& matrix, int x, int y) const {
    int n=matrix.size(), m=matrix[0].size(), mid_x=m/2, mid_y=n/2; //m Breite, n Höhe
    float res=0;
    for (int a=0, b=0; a<m; a++) {
        for (b=0; b<n; b++) {
            res+=(*this)(a-mid_x+x, b-mid_y+y)*matrix[a][b];
        }
    }
    return res;
}

float GreyScale::Clamp_(int x, int y) const {
    float tmp=(*this)(x,y);
    if (tmp>1)
        return 1;
    if (tmp<0)
        return 0;
    return tmp;
}

float GreyScale::Median_(int x, int y) const {
    vector<float> neighbours(9);
    for (int i=0, j=0; i<3; i++) {
        for (j=0; j<3; j++) {
            neighbours[3*i+j]=(*this)(x+i-1,y+j-1);
        }
    }
    sort(neighbours.begin(), neighbours.end());
    return neighbours[4];
}

GreyScale:: GreyScale (int width, int height, int depth):matrix(width,vector<float>(height,0)) {
    Resize(width, height, depth);
}

int GreyScale::GetWidth() const {
    return width;
}

int GreyScale::GetHeight() const {
    return height;
}

float GreyScale::operator()(int x, int y) const {
    if (x>=width) {
        x=width-1;
    }
    else if (x<0) {
        x=0;
    }
    if (y>=height) {
        y=height-1;
    }
    else if (y<0) {
        y=0;
    }
    return matrix[x][y];
}

float& GreyScale::operator()(int x, int y) {
    if (x>=width) {
        x=width-1;
    }
    else if (x<0) {
        x=0;
    }
    if (y>=height) {
        y=height-1;
    }
    else if (y<0) {
        y=0;
    }
    return matrix[x][y];
}

GreyScale& GreyScale::operator=(const GreyScale& other) {
    /*if ( &other == this)
    	return this;*/
    Resize(other.width, other.height, other.depth);
    for (int i=0; i<width; i++) {
        matrix[i]=other.matrix[i];
    }
}

GreyScale& GreyScale::operator+=(const GreyScale& other) {
    if (other.height!=height || other.width!=width) { //Größenanpassung mit Überschreiben des anderen Bildes?
        Resize(other.width, other.height);
    }
    for (int i=0, j=0; i<width; i++) {
        for (j=0; j<height; j++) {
            matrix[i][j]+=other.matrix[i][j];
        }
    }
}

GreyScale& GreyScale::operator-=(const GreyScale& other) {
    if (other.height!=height || other.width!=width) { //Größenanpassung mit Überschreiben des anderen Bildes?
        Resize(other.width, other.height);
    }
    for (int i=0, j=0; i<width; i++) {
        for (j=0; j<height; j++) {
            matrix[i][j]-=other.matrix[i][j];
        }
    }
}

void GreyScale::Resize(int width, int height, int depth) {
    matrix.resize(width, vector<float>(height));
    this->width=width;
    this->height=height;
    this->depth=depth;

}

GreyScale GreyScale::Binarize(float c) const {
    GreyScale result(width, height, depth);
    float tmp=0;
    for (int x=0,y=0; x<width; x++) {
        for (y=0; y<height; y++) {
            tmp=(*this)(x,y);
            if (tmp<c) {
                result(x,y)=0;
            }
            else {
                result(x,y)=1;
            }
        }
    }
    return result;
}

float GreyScale::Blur_(int x,int y) const {
    vector<vector<float>> blurMatrix(3,vector<float>(3));
    blurMatrix[0][0]=blurMatrix[0][2]=blurMatrix[2][0]=blurMatrix[2][2]=0;
    blurMatrix[0][1]=blurMatrix[2][1]=blurMatrix[1][2]=blurMatrix[1][0]=blurMatrix[1][1]=1;
    float gefaltet=faltung(blurMatrix,x,y);
    gefaltet/=5.0;
    return gefaltet;

}

GreyScale GreyScale::Blur() const {
    return wende_an('B');
}

GreyScale GreyScale::Clamp() const {
    return wende_an('c');
}

GreyScale GreyScale::Contrast() const {
    float max=(*this)(0,0), min=max, tmp;
    for (int x=0, y=0; x<width; x++) {
        for (y=0; y<height; y++) {
            tmp=(*this)(x,y);
            if (tmp<min) {
                min=tmp;
            }
            if (tmp>max) {
                max=tmp;
            }
        }
    }
    return LinTrans(1/(max-min),-min/(max-min));
}

GreyScale GreyScale::Convolve(const float mask[], int size) const {
    vector<vector<float>> matrix(size,vector<float>(size,0));
    for (int a=0, b=0; a<size; a++) {
        for (b=0; b<size; b++) {
            matrix[b][a]=mask[3*(b-size/2)+a-size/2];
        }
    }
    GreyScale result(width, height, depth);
    for (int x=0, y=0; x<width; x++) {
        for (y=0; y<height; y++) {
            result(x,y)=faltung(matrix,x,y);
            //result(x,y)=result.Clamp_(x,y);
        }
    }
    return result;
}

float GreyScale::Kirsch_(int x, int y) const {
    vector<vector<float>> kirschMatrix(3,vector<float>(3));
    kirschMatrix[0][0]=kirschMatrix[2][1]=1;
    kirschMatrix[0][1]=kirschMatrix[2][2]=-1;
    kirschMatrix[0][2]=kirschMatrix[1][2]=-3;
    kirschMatrix[1][0]=kirschMatrix[2][0]=3;
    kirschMatrix[1][1]=0;
    float gefaltet=faltung(kirschMatrix,x,y);
    return gefaltet;
}

GreyScale GreyScale::Kirsch() const {
    return wende_an('k');
}

float GreyScale::Laplace_(int x, int y) const {
    vector<vector<float>> laplaceMatrix(3,vector<float>(3));
    laplaceMatrix[0][0]=laplaceMatrix[0][2]=laplaceMatrix[2][0]=laplaceMatrix[2][2]=0;
    laplaceMatrix[0][1]=laplaceMatrix[1][2]=laplaceMatrix[1][0]=laplaceMatrix[2][1]=-1;
    laplaceMatrix[1][1]=4;
    float gefaltet=faltung(laplaceMatrix,x,y);
    return gefaltet;
}

GreyScale GreyScale::Laplace() const {
    return wende_an('l');
}

GreyScale GreyScale::LinTrans(float a, float b) const {
    GreyScale result(width, height, depth);
    for (int x=0, y=0; x<width; x++) {
        for (y=0; y<height; y++) {
            result(x,y)=a*(*this)(x,y)+b;
            //result(x,y)=result.Clamp_(x,y);
        }
    }
    return result;
}

GreyScale GreyScale::Invert() const {
    return LinTrans(-1,1);
}

GreyScale GreyScale::Median() const {
    return wende_an('m');
}

float GreyScale::Sobel_(int x, int y) const {
    vector<vector<float>> DX(3,vector<float>(3)), DY(3, vector<float>(3));
    DX[0][0]=DX[0][2]=DY[0][2]=DY[2][2]=-1;
    DX[0][1]=DY[1][2]=-2;
    DX[1].assign(3,0);
    DY[1][1]=DY[2][1]=DY[0][1]=0;
    DX[2][0]=DX[2][2]=DY[0][0]=DY[2][0]=1;
    DX[2][1]=DY[1][0]=2;
    double f1=faltung(DX,x,y), f2=faltung(DY,x,y);
    return (float)(sqrt(f1*f1+f2*f2));
}

GreyScale GreyScale::Sobel() const {
    return wende_an('s');
}

//ab jetzt Teil2

byte GreyScale::readNextHuffman(istream& istr) {
    char currentChar=istr.get();
    /*if (currentChar=='#') { //Ignoriere Kommentare
        while (!istr.fail()&&!istr.eof()&&currentChar!=10/*Line Feed) {
            //currentString+=currentChar;
              currentChar=istr.get();
        }
        istr>>ws;
        currentChar=istr.get();
    }*/
    byte currentByte=(byte)(currentChar);
    return currentByte;
}

int GreyScale::toInt(byte first, byte second, byte third, byte fourth) const{
	int first0=((int)(first))<<24;
	int second0=((int)(second))<<16;
	int third0=((int)(third))<<8;
	int fourth0=fourth;
	return first0+second0+third0+fourth0;
}

istream& operator>> (istream& istr, GreyScale& in) {
    string currentLine="";
    currentLine+=in.readNextHuffman(istr);
    currentLine+=in.readNextHuffman(istr);
    char drittes=in.readNextHuffman(istr);
    if(!isspace(drittes)){
    	 currentLine+=drittes;
    }
    if (currentLine=="P2"){
    	int width=stoi(in.readNext(istr)),
        height=stoi(in.readNext(istr)),
		depth=stoi(in.readNext(istr));
 		in.Resize(width,height,depth);
	for (int x=0,y=0; y<in.height; y++) {
	        x=0;
	        for (; x<in.width; x++) {
  		          in(x,y)=stoi(in.readNext(istr))/(float)depth;
   		}
   	    }
    } else if (currentLine=="P5"){
    	int width=stoi(in.readNext(istr)),
        height=stoi(in.readNext(istr)),
		depth=stoi(in.readNext(istr));
 		in.Resize(width,height,depth);
	for (int x=0,y=0; y<in.height; y++) {
	        x=0;
	        for (; x<in.width; x++) {
  		          in(x,y)=(byte)istr.get()/(float)depth;
   		}
   	    }
    } else if (currentLine=="MHa"||currentLine=="MHb"){
    	//int dimensions=;
    	cout<<"MHa/MHb\n";
    	byte first, second, third=istr.get(), fourth=istr.get();
    	int width=in.toInt(0,0,third,fourth);
    	third=istr.get();
    	fourth=istr.get();
    	int height=in.toInt(0,0,third,fourth);
    	in.Resize(width,height,255);
    	//cout<<width<<", "<<height<<"\n";
    	map<byte,int> hist;
    	int aktuelle_Haeufigkeit;
    	for(int i=0; i<256; i++){
    		first=istr.get(); second=istr.get(); third=istr.get(); fourth=istr.get();
    		aktuelle_Haeufigkeit=in.toInt(first,second,third,fourth); 
    		if(aktuelle_Haeufigkeit!=0){
			hist[(byte)(i)]=aktuelle_Haeufigkeit;
		}
	}
	//cout<<"Histogramm eingelesen\n";
	list<Knoten*> Knotenliste=in.toList(hist);
	while (!Knotenliste.front()->combine_least_two(Knotenliste));
	in.lies_huffman_bild_ein(istr,*(Knotenliste.front())); 
	if (currentLine=="MHb"){
    		in.retransformiere();
    	}  	
    }  else {
    	cerr<<"Magic Number falsch!";
    }    
    return istr;
}

void GreyScale::lies_huffman_bild_ein(istream& istr, Knoten& codetree){
	bool richtung;
	byte current_byte=istr.get(), count=0;
	int y=0,x=-1;
	Knoten* current_node_ptr=&codetree;
	while (!istr.eof()){
		for (count=0; count<8; count++){
			richtung=(current_byte >> (7-count)) & 1;
			if (richtung==0){
				current_node_ptr=current_node_ptr->getP0();
			} else {
				current_node_ptr=current_node_ptr->getP1();
			}
			if (!(current_node_ptr->getP0())||!(current_node_ptr->getP1())){ //sollte immer beides gegeben sein
				if (x<width-1){
					x++;
				} else if (y<height-1){
					x=0;
					y++;
				} else { //Aus dem Bild rausgelaufen, falls nicht am Ende der Datei angekommen
					istr.peek();
					if (!istr.eof()){
						cerr<<"\n\nBildformat zu klein angegeben\n\n";
					}
					return;
				}
				(*this)(x,y)=current_node_ptr->getGrauwert()/(float)depth;
				current_node_ptr=&codetree; //Gehe wieder zum Anfang des Baums
				//cerr<<"("<<x<<","<<y<<")";
			}
		}
		current_byte=istr.get();
	}
	/*for (count=0; count<8; count++){ //letztes Byte
		richtung=(current_byte >> (7-count)) & 1;
		if (richtung==0){
			current_node_ptr=current_node_ptr->getP0();
		} else {
			current_node_ptr=current_node_ptr->getP1();
		}
		if (!(current_node_ptr->getP0())||!(current_node_ptr->getP1())){ //Die letzten Einträge
			if (x<width-1){
				x++;
			} else if (y<height-1){
				x=0;
				y++;
			}
			(*this)(x,y)=current_node_ptr->getGrauwert()/(float)depth;
			current_node_ptr=&codetree; //Gehe wieder zum Anfang des Baums (hier eigentlich unnötig)
			//break; //hier fertig
		}
	}*/
	if (y!=height-1 || x!=width-1){
		cerr<<"\n\nBildformat zu groß angegeben\n\n";
	}
}

float Knoten::decodiere(istream& istr) const{
	const Knoten* current_node_ptr=this;
	byte current_byte, richtung;
	bool fertig=false;
	char count;
	while (!fertig){
		fertig=true;
		current_byte=istr.get();
		for (count=7; count>=0; count--){
			richtung=(current_byte >> count) & 1;
			if (richtung==0){
				current_node_ptr=current_node_ptr->P0;
			} else {
				current_node_ptr=current_node_ptr->P1;
			}
			if (!(current_node_ptr->P0)||!(current_node_ptr->P1)){
				/*if ((byte)(current_byte<<(7-count)) == 0){*/ //Diese Bedingung ist nicht richtig!
					return current_node_ptr->Grauwert/255.0;
				/*} else {
					cerr<<"Code zu lang!\n";
					return 0;
				}*/
			} else if (count==0) {
				fertig = false;
			}
		}
	}
	cerr<<"Code zu kurz! \n";
	return 0;
}

void GreyScale::transformiere(){ //Ränder problematisch, sonst auch Abweichungen in Größenordnung 1 GW
	/*for (int x=this->width-1,y=this->height-1;y>-1; y--) {
        	x=this->width-1;
       		for (;x>-1; x--) {
			float number=4;
			float p00=(*this)(x-1,y-1), p10=(*this)(x,y-1), p20=(x+1,y-1),p01=(*this)(x-1,y); //Nummeriering in Matrix-Notation
			if(y==0){
				p00=0;
				p10=0;
				p20=0;
				number=1;
				if(x==0){
					continue;
				}
			}else if(x==0){
				p00=0;
				p01=0;
				number=2;
			}else if (x==width-1){ //rechter Rand, tritt nicht für y=0 auf
				p20=0;
				number=3;
			}
            		(*this)(x,y)-=(p00+p10+p20+p01)/number;
            		while ((*this)(x,y)>1){
            			(*this)(x,y)-=256/255.0;
            		}
            		while ((*this)(x,y)<0){
            			(*this)(x,y)+=256/255.0;
            		}
        }
    }*/
    int sumOfNeighbors = 0;
	int numOfSums = 0;
	float tmp;

	for(int iHeight = height-1; iHeight>= 0; iHeight--) {
		for(int iWidth = width-1;iWidth>=0; iWidth--) {
			numOfSums = 0;
			sumOfNeighbors = 0;
			if (((iHeight-1)>=0) && ((iWidth-1)>=0)) {
				sumOfNeighbors += (int)((*this)(iWidth-1,iHeight-1)*255+0.5);
				numOfSums++;
			}
			if ((iHeight-1)>=0) {
				sumOfNeighbors += (int)((*this)(iWidth,iHeight-1)*255+0.5);
				numOfSums++;
			}
			if (((iWidth+1) <= (width-1)) && ((iHeight-1)>=0)) {
				sumOfNeighbors += (int)((*this)(iWidth+1,iHeight-1)*255+0.5);
				numOfSums++;
			}
			if ((iWidth-1)>=0) {
				sumOfNeighbors += (int)((*this)(iWidth-1,iHeight)*255+0.5);
				numOfSums++;
			}
			if (numOfSums!=0) {
				sumOfNeighbors = sumOfNeighbors/numOfSums; // abrunden
			}

			sumOfNeighbors = (int)((*this)(iWidth,iHeight)*255+0.5) - sumOfNeighbors;

			sumOfNeighbors = ((sumOfNeighbors % 256) + 256) % 256;
			(*this)(iWidth,iHeight) = sumOfNeighbors/255.0;
		}
	}
}  

void GreyScale::retransformiere(){ //Ränder problematisch, sonst auch Abweichungen in Größenordnung 1 GW
	/*for (int x=0,y=0;y<this->height; y++) {
        	x=0;
       		for (;x<this->width; x++) {
			float number=4;
			float p00=(*this)(x-1,y-1), p10=(*this)(x,y-1), p20=(x+1,y-1),p01=(*this)(x-1,y);
			if(y==0){
				p00=0;
				p10=0;
				p20=0;
				number=1;
				if(x==0){
					continue;
				}
			}else if(x==0){
				p00=0;
				p01=0;
				number=2;
			} else if(x==width-1){ //rechter Rand, tritt nicht für y=0 auf
				p20=0;
				number=3;
			}
            		(*this)(x,y)+=(p00+p10+p20+p01)/number;
            		while ((*this)(x,y)>1){
            			(*this)(x,y)-=256/255.0;
            		}
            		while ((*this)(x,y)<0){
            			(*this)(x,y)+=256/255.0;
            		}
        	}
    	}*/
    int numOfSums;
    int sumOfNeighbors;
    float tmp;
	for (int iHeight = 0; iHeight < height; iHeight++) {
		for (int iWidth = 0; iWidth < width; iWidth++) {
			numOfSums = 0;
			sumOfNeighbors = 0;
			if (((iHeight-1)>=0) && ((iWidth-1)>=0)) {
				tmp =(*this)(iWidth-1, iHeight-1)*255+0.5;
				sumOfNeighbors += (int)tmp;
				numOfSums++;
			}
			if ((iHeight-1)>=0) {
				tmp =(*this)(iWidth, iHeight-1)*255+0.5;
				sumOfNeighbors += (int)tmp;
				numOfSums++;
			}
			if (((iWidth+1) <= (width-1)) && ((iHeight-1)>=0)) {
				tmp =(*this)(iWidth+1,iHeight-1)*255+0.5;
				sumOfNeighbors += (int)tmp;
				numOfSums++;
			}
			if ((iWidth-1)>=0) {
				tmp =(*this)(iWidth-1,iHeight)*255+0.5;
				sumOfNeighbors += (int)tmp;
				numOfSums++;
			}
			if (numOfSums!=0) {
				tmp = ((float)sumOfNeighbors)/numOfSums;
				sumOfNeighbors = tmp; // abrunden
			}

			tmp = ((*this)(iWidth,iHeight)*255+0.5 + sumOfNeighbors);
			sumOfNeighbors = (int)tmp;
			sumOfNeighbors = sumOfNeighbors % 256;

			//cout << g(iWidth, iHeight) << " ";
			// g(iWidth, iHeight) = (g(iWidth,iHeight)*255+0.5 - sumOfNeighbors) % 256;
			(*this)(iWidth, iHeight) = ((float)sumOfNeighbors)/255;

		}
		//cout << endl;
	}
}  

ostream& operator<< (ostream& ostr, const GreyScale& out1) {
    GreyScale out=out1.Clamp();
    string MagicNumber;
    switch (GreyScale::format){
    	case 0 : MagicNumber="P2\n"; break;
    	case 1 : MagicNumber="P5\n"; out.depth=255; break;
    	case 2 : MagicNumber="MHa"; out.depth=255; break;
    	case 3 : MagicNumber="MHb"; out.depth=255; break;
    	default : cerr<<"Format nicht korrekt!";
    		return ostr; break;
    }
    ostr<<MagicNumber;
    out.proportions(ostr);
    if (GreyScale::format==3){
    	 out.transformiere();
    }
    if (GreyScale::format==2 || GreyScale::format==3)
    {
    	out.huffman(ostr); //Histogramm anlegen und reinschreiben
    }
    out.ausgabe(ostr);
    return ostr;
}

list<byte> GreyScale::toBytes(int toConvert) const{
	list<byte> result;
	byte i=0;
	while (toConvert!=0 || i<4){
		result.emplace_front((byte)toConvert);
		toConvert=toConvert>>8;
		i++;
	}
	return result;
}

ostream& GreyScale::proportions(ostream& ostr) const{
	switch (GreyScale::format){
		case 0 : 
		case 1 : ostr<<width<<" "<<height<<" "<<depth<<"\n"; break;
		case 2 : 
		case 3 : 
			list<byte> as_bytes=toBytes(width);
			ostr<<*(++as_bytes.rbegin())<<as_bytes.back();
			as_bytes=toBytes(height);
			ostr<<*(++as_bytes.rbegin())<<as_bytes.back();
			break;
	}
	ostr.flush();
	return ostr;
}
    
ostream& GreyScale::ausgabe(ostream& ostr){
	//neu:
	if (GreyScale::format==0 || GreyScale::format==1){
		for (int y=0, x, count; y<height; y++){
			for (x=0; x<width; x++){
				count++;
				if (GreyScale::format==0){
					ostr<<(int)(depth*((*this)(x,y)))<<" ";
					if (count>=70){
						ostr<<"\n";
						count=0;
					}
				} else {
					ostr<<(byte)(depth*((*this)(x,y)));
				}
			}
		}
	} else { //format 2,3
		byte cur_byte=0, count=0;
		for (int y=0, x; y<height; y++){
			for (x=0; x<width; x++){
				for (bool current_value : code[(byte)round(depth*((*this)(x,y)))]){
					if (count>=8){
						count=0;
						ostr<<cur_byte;
						cur_byte=0;
					}
					cur_byte=cur_byte<<1;
					cur_byte+=current_value;
					count++;
				}
			}
		}
		cur_byte=cur_byte<<(8-count); //Fülle mit 0 auf
		ostr<<cur_byte;
	}
}

Knoten:: Knoten(byte Grauwert, int Haeufigkeit){
	this->Grauwert=Grauwert;
	this->Haeufigkeit=Haeufigkeit;
	this->P0=NULL;
	this->P1=NULL;
}
	
Knoten:: Knoten(byte Grauwert, int Haeufigkeit, Knoten& P0, Knoten& P1){
	this->Grauwert=Grauwert;
	this->Haeufigkeit=Haeufigkeit;
	this->P0=&P0;
	this->P1=&P1;
}
	
Knoten:: ~Knoten(){
	/*if (P0)
		delete P0;
	if (P1)
		delete P1;
	*/
}
	
Knoten Knoten::vereinigung(Knoten& P, Knoten& Q) const{
	Knoten result;
	result.Haeufigkeit=P.Haeufigkeit+Q.Haeufigkeit;
	result.Grauwert=(int)min(P.Grauwert,Q.Grauwert);
	int hp=P.Haeufigkeit, hq=Q.Haeufigkeit;	
	if (hp>hq){
		result.P0=&Q;
		result.P1=&P;
	} else if (hp<hq){
		result.P0=&P;
		result.P1=&Q;
	} else if (P.Grauwert<Q.Grauwert){
		result.P0=&P;
		result.P1=&Q;
	} else {
		result.P0=&Q;
		result.P1=&P;
	}
	
	return result;
}
/**
* @brief Unifies the two least nodes in the list
* @param nodes The list
* @return true if the list contains exactly one element, else false
*/
bool Knoten::combine_least_two(list<Knoten*>& nodes) const{ //List bietet sich an
	int size=nodes.size();
	if (size==0){
		cerr<<"\nFehler, Liste leer!\n";
		return true;
	}
	if (size==1){
		return true;
	}
	auto smallest=nodes.begin(), second_smallest=++nodes.begin();
	for (auto current_ptr=smallest; current_ptr!=nodes.end(); ++current_ptr){
		if ((*current_ptr)->Haeufigkeit==0){
			cerr<<"Häufigkeit 0 aufgetaucht!\n";
		}
		if ((*current_ptr)->Haeufigkeit < (*smallest)->Haeufigkeit || 
			((*current_ptr)->Haeufigkeit == (*smallest)->Haeufigkeit && (*current_ptr)->Grauwert < (*smallest)->Grauwert)
			){
			second_smallest=smallest;
			smallest=current_ptr;
		} else if(((*current_ptr)->Haeufigkeit < (*second_smallest)->Haeufigkeit || 
			((*current_ptr)->Haeufigkeit == (*second_smallest)->Haeufigkeit && (*current_ptr)->Grauwert < (*second_smallest)->Grauwert) )&& current_ptr!=smallest
		){
			second_smallest=current_ptr;
		}
	}
	Knoten* vereint= new Knoten;
	*vereint = vereinigung((**smallest),(**second_smallest));
	nodes.push_front(vereint);
	nodes.remove(*smallest); //so bleiben (hoffentlich) die Iteratoren erhalten
	nodes.remove(*second_smallest);
	/*for (auto current_node_ptr : nodes){
		cout<<"_______\n"<<*current_node_ptr<<"----------\n";
	}
	cout<<"\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n";*/
	return false;
}

map<byte,int> GreyScale::histogramm() const{
	map<byte,int> count;
	byte entry;
	auto map_pos=count.begin();
	for (int x=0, y; x<width; x++){
		for (y=0; y<height; y++){
			entry=(byte)round(depth*(*this)(x,y));
			map_pos=count.find(entry);
			if (map_pos==count.end()){
				count[entry]=1;
			} else {
				map_pos->second++; //Erhöhe den Eintrag (jetzt auf jeden Fall existent) um eins
			}
		}
	}
	return count;
}

list<Knoten*> GreyScale::toList(map<byte,int>& count){
	list<Knoten*> result;
	for (auto current_pair : count){ //Fülle die Liste
		Knoten* dazu=new Knoten(current_pair.first, current_pair.second);
		result.push_front(dazu);
		//cout<<"toList\n";
	}
	//cout<<"toList Ende\n";
	return result;
}

void Knoten::add_codes(map<byte,vector<bool>>& codes, const vector<bool>& codepart) const{
	if (P0){
		vector<bool> cp1(codepart);
		cp1.push_back(0);
		P0->add_codes(codes,cp1);
	}
	if (P1){
		vector<bool> cp2(codepart);
		cp2.push_back(1);
		P1->add_codes(codes,cp2);
	}
	if (!P0 && !P1){
		codes[Grauwert]=codepart;
	}
}

list<byte> GreyScale::vervollstaendigen(const vector<bool>& oldcode) const{ //jetzt eigentlich unnötig
	list<byte> newcode;
	byte current_byte=0, count=0;
	for (bool wert: oldcode){
		if (count>=8){
			newcode.push_back(current_byte);
			count=0;
			current_byte=0; //sollte man eigentlich nicht müssen
		}
		current_byte=current_byte<<1;
		current_byte+=wert;
		count++;
	}
	current_byte=current_byte<<(8-count); //Fülle mit 0 auf
	newcode.push_back(current_byte);
	return newcode;
}

void GreyScale::codierung(Knoten& tree){
	//map<byte,vector<bool>> codes;
	tree.add_codes(code,vector<bool>());
	/*map<byte,list<byte>> result;
	for (auto oldcode : codes){
		code[oldcode.first]=vervollstaendigen(oldcode.second);
		cout<<"("<<dec<<(int)(oldcode.first)<<", ";
		for (bool codepart: oldcode.second){
			cout<<(int)codepart<<", ";
		}
		cout<<"|";
		for (byte codepart : code[oldcode.first]){
			cout<<hex<<(int)codepart<<", ";
		}
		cout<<")\n";
		cout.flush();
	}*/
}

/**
* @brief
*/
void GreyScale::huffman(ostream& ostr){
	map<byte,int> histogramm=this->histogramm(); //histogramm bestimmen
	int current_count=0;
	auto current_pointer=histogramm.begin();
	//reinschreiben
	for (byte i=0; i<depth; i++){
		current_pointer=histogramm.find(i);
		if (current_pointer==histogramm.end()){
			current_count=0;
		} else {
			current_count=current_pointer->second;
		}
		for (byte current_byte : toBytes(current_count)){
			ostr<<current_byte;
		}
	}
	current_pointer=histogramm.find(255);
	if (current_pointer==histogramm.end()){
		current_count=0;
	} else {
		current_count=current_pointer->second;
	}
	for (byte current_byte : toBytes(current_count)){
		ostr<<current_byte;
	}
	//code anlegen
	list<Knoten*> Knotenliste=toList(histogramm);
	while (!Knotenliste.front()->combine_least_two(Knotenliste));
	codierung(*(Knotenliste.front()));
	//delete Knotenliste.front();
}

bool Knoten::operator==(const Knoten& other){
	return this->P0==other.P0 && this->P1==other.P1 && this->Grauwert==other.Grauwert && this->Haeufigkeit==other.Haeufigkeit;
}

ostream& operator<<(ostream& ostr, Knoten& out){
	return out.ausgabe(ostr,"");
}

ostream& Knoten::ausgabe(ostream& ostr,string prefix) const{
	/*if (!P0 && !P1)*/{
		ostr<<prefix<<"("<<Haeufigkeit<<", "<<(int)Grauwert<<")\n";
	}
	if (P0){
		P0->ausgabe(ostr,prefix+"0");
	}
	if (P1){
		P1->ausgabe(ostr,prefix+"1");
	}
	return ostr;
}

Knoten* Knoten::getP0() const{
	return P0;
}

Knoten* Knoten::getP1() const{
	return P1;
}

byte Knoten::getGrauwert() const{
	return Grauwert;
}
