#include<fstream>
#include"greyscale.h"
#include"unit.h"
#include<string>

int main () {
    GreyScale pic1, pic2, newpic;
    fstream   file;
    string p;
    cout<<"Pfad Bild1? \n";
    cin>>p;
    file.open(p);
    file>>pic1;
    file.close();
    cout<<"Pfad Bild2? \n";
    cin>>p;
    file.open(p);
    file>>pic2;
    file.close();
    for (int i=0, j=0; i<pic1.GetWidth(); i++) {
        for (j=0; j<pic1.GetHeight(); j++) {
            if(pic1(i,j)!=pic2(i,j)) {
                cout<<"Fehler bei ("<<i<<","<<j<<") um "<<255*(pic1(i,j)-pic2(i,j))<<endl;
            }
        }
    }
    return 0;
}
