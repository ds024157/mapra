#include "unit.h"
#include <vector>
#include <map>
#include <string>
#include <cctype> //Für whitespaces
#include <iostream>
#include <cmath>
#include <algorithm> //Für sort
#include <list>

using namespace std;

class Knoten {
private :
    byte Grauwert ;
    int Haeufigkeit ;
    Knoten * P0 , * P1 ;

    Knoten() {};

    ostream& ausgabe(ostream&,string) const;

public: //Konstruktoren
    Knoten(byte Grauwert, int Haeufigkeit);

    Knoten(byte Grauwert, int Haeufigkeit, Knoten& P0, Knoten& P1);
    //Destruktor
    ~Knoten();
    //Selektoren
    Knoten* getP0() const;

    Knoten* getP1() const;

    byte getGrauwert() const;
    //Operatoren
    bool operator==(const Knoten& other);
    //restliche Funktionen
    Knoten vereinigung(Knoten& P0, Knoten& P1) const;

    void add_codes(map<byte,vector<bool>>& codes, const vector<bool>& codepart) const;

    bool combine_least_two(list<Knoten*>& nodes) const;

    float decodiere(istream& istr) const;
    //friends
    friend ostream& operator<<(ostream& ostr, Knoten& out);
};

/**
* \brief Klasse zum Darstellen von Graustufenbildern fester Größe, unterstützt Ein- und Ausgabe im pgm-Format
*/

class GreyScale {
private:
    int width,height,depth;
    vector<vector<float>> matrix;
    map<byte, vector<bool>> code;
    map<list<byte>, byte> decode;

    string readNext(istream& istr);

    GreyScale wende_an(char filter) const;

    float faltung(vector<vector<float>>& matrix, int x, int y) const;

    float Clamp_(int x, int y) const;

    float Median_(int x, int y) const;

    float Sobel_(int x, int y) const;

    float Blur_(int x,int y) const;

    float Kirsch_(int x, int y) const;

    float Laplace_(int x, int y) const;

    list<Knoten*> toList(map<byte,int>&);

    map<byte,int> histogramm() const;

    void codierung(Knoten& tree);

    list<byte> vervollstaendigen(const vector<bool>& oldcode) const;

    void huffman(ostream&);

    ostream& proportions(ostream& ) const;

    ostream& ausgabe(ostream&);

    list<byte> toBytes(int) const;

    void transformiere();

    void retransformiere();

    int toInt(byte,byte,byte,byte) const;

    byte readNextHuffman(std::istream&);

    void lies_huffman_bild_ein(istream&, Knoten&);

public:
    static int format;
//Konstruktoren
    /**
    * \brief Konstruktor, setzt ein leeres (schwarzes) Bild mit Breite und Höhe
    * \param width Die Breite des Bildes in Pixeln
    * \param height Die Höhe des Bildes in Pixeln
    * \param depth Die maximale Farbtiefe
    */
    GreyScale (int width=0, int height=0, int depth=0);

//Selektoren
    int GetWidth() const;

    int GetHeight() const;

    static void SetFormat(int i) {
        format=i;
    }

    int GetFormat() const {
        return format;
    }
    /**
    * \brief Returns the value of the pixel (x,y). If (x,y) is not in the picture the value of the closest pixel will be returned.
    * \param x The x-coordinate
    * \param y Thy y-coordniate
    * \return The value of the pixel (x,y). If (x,y) is not in the picture the value of the closest pixel will be returned.
    */
//Operatoren
    float operator()(int x, int y) const;

    float& operator()(int x, int y);

    /**
    * \brief Standard-Assign-Operator
    */
    GreyScale& operator=(const GreyScale& other);

    /**
    * \brief Bei verschiedenen Dimensionen wird die des linken angepasst
    */
    GreyScale& operator+=(const GreyScale& other);

    /**
    * \brief Bei verschiedenen Dimensionen wird die des linken angepasst
    */
    GreyScale& operator-=(const GreyScale& other);

//Weitere member-functions
    /**
    * \brief Setzt die Breite und Höhe des Bildes neu, das Bild ist danach schwarz
    * \param width Die neue Breite in Pixeln
    * \param height Die neue Höhe in Pixeln
    */
    void Resize(int width=0, int height=0, int depth=0);
//Filter
    GreyScale Binarize(float c) const;

    GreyScale Blur() const;

    GreyScale Clamp() const;

    GreyScale Contrast() const;

    GreyScale Convolve(const float mask[], int size=3) const;

    GreyScale Kirsch() const;

    GreyScale Laplace() const;

    GreyScale LinTrans(float a, float b) const;

    GreyScale Invert() const;

    GreyScale Median() const;

    GreyScale Sobel() const;

    friend istream& operator>> (istream& istr, GreyScale& in);
    friend ostream& operator<< (ostream& ostr, const GreyScale& out);
};

