#include <iostream>

using namespace std;

class A{
public: 
	int x;
	A(){
		x=10;
	}	
};

class B: public A {
public:
	B(){
		x=12;
	}
};

int f(A a){
	return a.x;
}

int main(){
	A Kla; 
	B Klb;
	cout<<f(Kla)<<f(Klb)<<"\n";
}
