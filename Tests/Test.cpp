#include <iostream>
#include <string>
#include <list>
	
using namespace std;

template <typename T>
T sum (T a, T b)
{
	return a+b;
}

struct dooferStruct{
	int a;
};

dooferStruct operator+ (dooferStruct a, dooferStruct b){
	dooferStruct res;
	res.a=a.a+b.a;
	return res;
}

std::ostream& operator<< (std::ostream& s, dooferStruct a){
	return s<<a.a;
}

int main(){
	int ia=1, ib=5;
	double da=1.7, db=98.9;
	dooferStruct sa,sb;
	sa.a=99; sb.a=100;
	cout<<sum(ia,ib)<<" "<<sum(da,db)<<" "<<sa+sb<<"\n";
	list<int> Beispielliste;
	Beispielliste.push_back(1);
	Beispielliste.push_back(2);
	for (auto i : Beispielliste)
		cout<<i<<"\n";
	return 0;
}
