#include "unit.h"
#include <fstream>
using namespace std;

template <typename ElemT>
void einlesen(std :: ifstream & ifs ,vector < ElemT >& feld ) {
    ElemT cur;
    while (!ifs.fail() && !ifs.eof()) {
        ifs>>cur;
        if (!ifs.fail() && !ifs.eof())
            feld.push_back(cur);
    }
}

template <typename ElemT>
void show(vector <ElemT>& feld) {
    for (int i=0; i<feld.size(); i++) {
        cout<<feld[i]<<"\n";
    }
}

template <typename ElemT>
void tausche(vector <ElemT> &Feld, unsigned int i, unsigned int j) {
    ElemT a = Feld[i];
    Feld[i] = Feld[j];
    Feld[j] = a;
}

template <typename ElemT>
void bubbleSort(vector <ElemT> &Feld)
{
    unsigned int laenge=Feld.size();
    bool fertig=true;//falls in einem durchlauf von j nicht getauscht, ist das Array sortiert
    for (unsigned int i=0,j=laenge; i<laenge; i++)//läuft von links
    {
        fertig=true;
        for (j=laenge-1; j>i; j--)//läuft von rechts bis i
        {
            if (Feld[j]<Feld[j-1])
            {
                tausche(Feld,j,j-1);//bubble
                fertig=false;
            }
        }
        if (fertig)
            return;
    }
}

template <typename ElemT>
void selectionSort(vector<ElemT> &Feld)
{
    unsigned int laenge=Feld.size();
    for (unsigned int i=0,j=laenge,pos=0; i<laenge; i++)//Stellen, an die richtiges Element gesetzt wird
    {
        pos=i;
        for (j=i+1; j<laenge; j++)//sucht das Minimum im Restarray
        {
            if (Feld[j]<Feld[pos])
            {
                pos=j;
            }
        }
        tausche(Feld,pos,i);
    }
}

template <typename ElemT>
void merge(vector <ElemT> &Feld, vector <ElemT> &feld1, vector<ElemT> &feld2) {
    unsigned int laenge=Feld.size(), laenge1=feld1.size(), laenge2=feld2.size(), zeiger1=0, zeiger2=0;
    for (unsigned int i=0; i<laenge; i++) { //feld1 und feld2 sind sortiert, füge nun richtig zusammen
        if (zeiger1<laenge1) { //feld1 nicht "leer"
            if (zeiger2<laenge2) { //feld2 nicht "leer"
                if (feld1[zeiger1]<feld2[zeiger2]) //Falls beide nicht leer füge das Minimum in Feld ein
                    Feld[i]=feld1[zeiger1++]; //und erhöhe den Zähler in dem Array woher das Minimum kommt
                else
                    Feld[i]=feld2[zeiger2++];
            }
            else
                Feld[i]=feld1[zeiger1++];	//falls eins leer ist nimm das andere
        }
        else if (zeiger2<laenge2)
            Feld[i]=feld2[zeiger2++];
    }
}

template <typename ElemT>
void mergeSort(vector <ElemT> &Feld) {
    unsigned int laenge=Feld.size(), l1=laenge/2,l2=laenge-l1,i; //Teile mehr oder weniger gleichmäßig auf
    if (laenge<=1) //Grenzfall
        return;

    vector<ElemT> feld1(l1), feld2(l2);
    for (i=0; i<l1; i++) {	//Kopiere in die Teilarrays
        feld1[i]=Feld[i];
    }
    for (; i<laenge; i++) {
        feld2[i-l1]=Feld[i];
    }
    mergeSort(feld1); //Sortiere rekursiv
    mergeSort(feld2);
    merge(Feld,feld1,feld2); //merge
}

int main() {
    vector<Student> StudentenFeld;
    vector<string> StringFeld;
    vector<double> DoubleFeld;
    ifstream ifs("studenten.txt");
    einlesen(ifs, StudentenFeld);
    //show(StudentenFeld);
    ifs.close();
    ifs.open("doubles.txt");
    einlesen(ifs, DoubleFeld);
    //show(DoubleFeld);
    ifs.close();
    ifs.open("strings.txt");
    einlesen(ifs, StringFeld);
    ifs.close();
    //show(StringFeld);
    cout<< "Welcher Algorithmus? (B=Bubblesort, A=Auswahlsortieren, M=Mergesort)\n";
    char a=' ';
    while (a!='B'&&a!='A'&&a!='M') {
        cin>>a;
        if (a!='B'&&a!='A'&&a!='M')
            cout<<"Bitte eins der Zeichen eingeben (case-sensitive)\n";
    }
    switch (a) {
    case 'B' :
        bubbleSort(StudentenFeld);
        bubbleSort(StringFeld);
        bubbleSort(DoubleFeld);
        break;
    case 'A' :
        selectionSort(StudentenFeld);
        selectionSort(StringFeld);
        selectionSort(DoubleFeld);
        break;
    case 'M' :
        mergeSort(StudentenFeld);
        mergeSort(StringFeld);
        mergeSort(DoubleFeld);
        break;
    }
    ergebnis(StudentenFeld);
    ergebnis(DoubleFeld);
    ergebnis(StringFeld);
    return 0;
}
