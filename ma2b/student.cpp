#include "student.h"

// Eingabeoperator ">>"
std::istream& operator>> ( std::istream& s, Student& a)
{
    s >> a.Vorname >> a.Nachname >> a.MatNr >> a.Note;
    return s;
}

// Ausgabeoperator "<<"
std::ostream& operator<< ( std::ostream& s, const Student& a)
{
    s << a.Vorname <<" "<< a.Nachname<<" "<< a.MatNr<<" " << a.Note;
}

// Vergleichsoperator "<"
bool operator< ( const Student& s1, const Student& s2)
{
    int vergleich=s1.Nachname.compare(s2.Nachname);
    if (vergleich<0)
        return true;
    else if (vergleich>0)
        return false;
    else {
        vergleich=s1.Vorname.compare(s2.Vorname);
        if (vergleich<0)
            return true;
    }
    return false;
}

// Vergleichsoperatoren "==" bzw. "!="
bool operator== ( const Student& s1, const Student& s2)
{
    return s1.Nachname==s2.Nachname&&s1.Vorname==s2.Vorname;
}

bool operator!= ( const Student& s1, const Student& s2)
{
    return !(s1==s2);
}

bool operator<= ( const Student& s1, const Student& s2)
{
    return s1<s2||s1==s2;
}

bool operator>= ( const Student& s1, const Student& s2)
{
    return s1>s2||s1==s2;
}

bool operator> ( const Student& s1, const Student& s2)
{
    return !(s1<=s2);
}


