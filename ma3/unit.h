/*****************************************************************
*  Name       : unit.h                                           *
*  Verwendung : Schnittstelle zu Praktikumsumgebung (MAPRA),     *
*               Distanzberechnung mit Algorithmus von Dijkstra   *
*  Autor      : Sven Gross, IGPM RWTH Aachen                     *
*  Datum      : Juni 2003                                        *
*****************************************************************/

#ifndef _UNIT
#define _UNIT

#include <vector>
#include <list>
#include <utility>

typedef int                         VertexT;
typedef std::pair<VertexT,VertexT>  EdgeT;
typedef double                      CostT;

extern const double infty;

void Ergebnis (const int Bsp, const VertexT v0, const std::vector<CostT>& D);

void PruefeWeg(const int Bsp, const std::list<VertexT>& weg);

#endif
