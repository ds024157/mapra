/*****************************************************************
*  Name       : dijkstra.cpp                                     *
*  Verwendung : Loesung zur MAPRA-Aufgabe zur                    *
*               Distanzberechnung mit Algorithmus von Dijkstra   *
*  Autor      :                                                  *
*  Datum      :                                                  *
*****************************************************************/

#include "unit.h"
#include <fstream>
#include <map>
#include <iostream>
//#include <unordered_map>

using namespace std;


class DistGraph
// Klasse zur Repraesentation eines Distanzgraphen
{
private:
    // die eigentlichen Datenelemente fehlen noch...
    unsigned int NV, NE;
    /*unordered_*/map <EdgeT, CostT> c;
    //vector<vector<CostT>> c(N,vector(N,infty)); //The Matrix containing the Edgelengths for Dijkstra

    DistGraph(const int num_verts, const int num_edges, /*unordered_*/map<EdgeT, CostT> distances) {
        NV=num_verts;
        NE=num_edges;
        c=distances;
    }


public:
    DistGraph(const int num_verts= 0) {

    }

    int GetNumVertices() const {
        return NV;
    }
    int GetNumEdges   () const {
        return NE;
    }

    bool ContainsVertex(const VertexT v) const {
        return  (v<NV && v>=0);
    }
    bool ContainsEdge  (const EdgeT e)   const {
        auto pointer=c.find(e);
        return pointer!=c.end();
    }

    CostT beta(const EdgeT e)      const;
    CostT Cost(const VertexT from, const VertexT to) const {
        if (from==to)
            return 0;
        auto res=c.find(EdgeT(from,to));
        if (res==c.end()) //Edge not in Map
            return infty;
        return (*res).second; //res is the pointer to the pair in c
    }

    void InsertEdge(const EdgeT e, const CostT c);

    friend istream& operator >> ( istream&, DistGraph&);
    friend ostream& operator << ( ostream&, DistGraph&);
};

istream& operator >> (istream& istr, DistGraph& graph) {
    istr >> graph.NV >> graph.NE;
    /*unordered_*/map<EdgeT,CostT> c;
    EdgeT e;
    CostT cost;
    for (unsigned int i=0; i<graph.NE; i++) { //Assuming the file is correct
        istr >> e.first >> e.second >> cost;
        if (!istr.fail()&&!istr.eof())
            c.insert(pair<EdgeT,CostT>(e,cost));
    }
    graph.c=c;
}

ostream& operator << (ostream& ostr, DistGraph& graph) {
    ostr<<graph.NV<<"\n"<<graph.NE<<"\n";
    for (pair<EdgeT,CostT> p : graph.c) {
        ostr<< p.first.first << " " << p.first.second << "\t" << p.second << "\n";
    }
}

template <typename T>
void show(vector<T> v) {
    cout<<"[ ";
    for (auto i : v) {
        cout<<i<<", ";
    }
    cout<<"]\n";
}

vector<CostT> Dijkstra_Alg(VertexT v0, const DistGraph& graph, vector<VertexT>& path) {
    unsigned int NV=graph.GetNumVertices();
    VertexT v;
    CostT tmpCost;
    vector<bool> S(NV,false);
    vector<CostT> D(NV);
    path.assign(NV,-1);
    /*cout<<"Path in Dijkstra\n";
    show(path);*/
    bool loop=true;
    S[v0]=true;
    for (v=0; v<NV; v++) {
        D[v]=graph.Cost(v0,v);
        if (D[v]!=infty)
            path[v]=v0;
    }
    while (loop) {
        unsigned int v1=0;
        loop=false; //will stay false if all Elements in S are false
        for (v=0; v< NV; v++) {
            if (!S[v]) {
                if (!loop) {
                    v1=v; //Initialize v1 with a matching value
                }
                if (D[v]<D[v1]) {
                    v1=v;
                }
                loop=true;
            }
        }
        if (!loop)
            break; //No unregarded vertices left
        S[v1]=true;
        for (v=0; v<NV; v++) {
            if (S[v])
                continue;
            tmpCost=D[v1]+graph.Cost(v1,v);
            if (D[v]>tmpCost) {
                path[v]=v1;
                D[v]=tmpCost;
            }
        }
    }
    return D;
}

vector<list<VertexT>> generate_paths(const vector<VertexT> pathVector) {
    vector<list<VertexT>> paths(pathVector.size());
    for (int v=0,w=0; v<pathVector.size(); v++) {
        w=v;
        do {
            if (w==-1) {
                paths[v].clear();
                paths[v].push_back(v);
                break;
            }
            paths[v].push_front(pathVector[w]);
            w=pathVector[w];
        } while(w!=pathVector[w]);
    }
    return paths;
}

int main() {
    cout << "Welcher Graph soll betrachtet werden (zwischen 1 und 5)?\n";
    unsigned int i=0;
    VertexT v0,v1;
    vector<VertexT> pathVector;
    vector<list<VertexT>> paths;
    while (i<1||i>5) {
        cin>>i;
        if (i<1||i>5)
            cout<<"Bitte eins der Zeichen eingeben \n";
    }
    ifstream ifs;
    DistGraph Graph;
    switch (i) {
    case 1:
        ifs.open("Graph1.dat");
        break;
    case 2:
        ifs.open("Graph2.dat");
        break;
    case 3:
        ifs.open("Graph3.dat");
        break;
    case 4:
        ifs.open("Graph4.dat");
        break;
    case 5:
        ifs.open("Graph5.dat");
        break;
    }
    ifs >> Graph;
    ifs.close();
    //cout << Graph<<"\n";
    if (i!=5) {
        for (v0=0; v0<Graph.GetNumVertices(); v0++) {
            Ergebnis(i, v0, Dijkstra_Alg(v0,Graph, pathVector));
            //show(pathVector);
            paths=generate_paths(pathVector);
            for (v1=0; v1<Graph.GetNumVertices(); v1++) {
                PruefeWeg(i,paths[v1]);
            }
        }
    } else {
        do {
            cout << "Beispiel 5 ist zu lang um alle Knoten auszugeben. Bitte v0 angeben\n";
            cin >> v0;
        } while (cin.fail());
        Ergebnis(5,v0,Dijkstra_Alg(v0,Graph, pathVector));
        paths=generate_paths(pathVector);
        for (v1=0; v1<Graph.GetNumVertices(); v1++) {
            PruefeWeg(i,paths[v1]);
        }
    }
}
