/****************************************************************
*  Name       : rsa.cpp                                         *
*  Verwendung :                                                 *
*  Autor      :                                                 *
*  Datum      :                                                 *
*  Sprache    : C++                                             *
****************************************************************/

#include <iostream>
#include <fstream>
#include <ctime>
#include <string>
#include <map>
#include <cmath>


#include "unit.h"

//

//-----------------------------------------------------------------------------
// Klasse Prim enthaelt die ersten n = 6542 Primzahlen enthaelt,
// (nachdem Sie die Funktion Prim::init() geschrieben haben)
//-----------------------------------------------------------------------------
Prim prim;
void Prim::init() {
    bool kaputt;
    pMenge.insert(2);
    for (uint64_t i=3; i<pMax; i++) {
        kaputt=false;
        for (uint64_t p : pMenge) {
            if (p*p>i) {
                break;
            }
            if (i % p == 0) {
                kaputt=true;
                break;
            }
        }
        if (!kaputt) {
            pMenge.insert(i);
        }
    }
}

// Funktionen ggt, xggt, fastpow ...
uint64_t ggt(uint64_t a, uint64_t b)
{
    if(b==0) {
        return a;
    } else {
        return ggt(b, a % b);
    }
}

uint64_t xggt(uint64_t a, uint64_t b, int64_t & x, int64_t & y)
{
    if (b==0) {
        x=1;
        y=0;
        return a;
    }
    uint64_t res=xggt(b, a % b, x ,y);
    int64_t xn=y,yn=x-y*(int64_t)(a/b);
    x=xn;
    y=yn;
    return res;

}

uint64_t fastpow(uint64_t nBase, uint64_t nExp, uint64_t nMod)
{
    uint64_t prod=1;
    uint64_t aktFakt=nBase;
    //int last=0;
    //int diff;
    char xi;
    for(int i=0, j=0; i<64; i++) {
        xi=(nExp>>i)%2;
        if(xi==1) {
            prod=(prod * aktFakt) % nMod;
        }
        aktFakt=(aktFakt*aktFakt) % nMod ;
    }
    return prod;
}

// Klassenfunktionen ...

std::ostream & operator<<(std::ostream & os, const RSA & pp) {
    os<<pp.n<<pp.e;
    return os;
}
std::istream & operator>>(std::istream & is, RSA & pp) {
    is>>pp.n>>pp.e;
    return is;
}

uint64_t RSA::Verschluesseln(const uint64_t & m, bool bPublic) const
{
    if (bPublic) {
        return fastpow(m, e, n);
    } else {
        return fastpow(m, d, n);
    }
}

uint64_t RSA::Entschluesseln(const uint64_t & c, bool bPublic) const
{
    return Verschluesseln(c,bPublic);
}

void RSA::NeueSchluessel()
{
    Prim prim;
    uint64_t size=prim.length();
    int64_t y=0,d1,mask = (1<<15)+1;
    //std::cerr<<"\n"<<mask<<"\n";
    p=q=0;
    while (!prim[p]/*||p==2*/) {
        p=rand() % size;
        p|=mask;
    }
    while (!prim[q] || q==p /*|| p*q<=256*/) {
        q=rand() % size;
        q|=mask;
    }
    n=p*q;
    phin=(p-1)*(q-1);
    do {
        d1=1;
        y=0;
        e=rand() % (phin-2) +2;
    } while (xggt(e,phin,d1,y)!=1);
    while (d1<=1) {
        d1+=phin;
    }
    while (d1>=phin) {
        d1-=phin;
    }
    //std::cerr<<"\np= "<<p<<", q= "<<q<<", d="<<d1<<", e= "<<e<<"\n";
    if (d1==0 || d1==1||(d1*e)%phin!=1) {
        std::cerr<<"\n Invertieren hat nicht geklappt, Schlüssel nicht sicher!\n";
        return;
    }
    d=(uint64_t)d1;
}


int main()
{
    srand(time(NULL));
    //Pruefe_ggt(ggt);
    //Pruefe_xggt(xggt);
    //Pruefe_fastpow(fastpow);

    // neue Schluessel generieren
    RSA rsa;
    rsa.NeueSchluessel();
    // PruefeSchluessel
    PruefeSchluessel(rsa);

    // Text und Signatur erstellen
    std::string Nachricht="Die Wurzel aus 2 ist irrational";
    Signatur sig;
    sig.set(Nachricht);
    // Signatur Verschluesseln
    for (int i=0; i<sig.length(); i++) {
        sig[i]=rsa.Verschluesseln(sig[i],false);
    }
    // SchickeNachricht
    SchickeNachricht(Nachricht,rsa,sig);
    // Map definieren
    std::map <std::string,RSA> keys;
    // Keys einlesen
    std::ifstream istr("keys.txt");
    std::string Name;
    while (!istr.eof()) {
        istr>>Name;
        istr>>rsa;
        keys[Name]=rsa;
    }
    istr.close();
    // ueber alle Eintraege in der Map iterieren
    Signatur sigN;
    bool unveraendert;
    for (auto cur_pair:keys) {
        unveraendert=true;
        // HoleNachricht
        HoleNachricht(cur_pair.first,Nachricht,sig);
        sigN.set(Nachricht);
        if (sig.length()!=sigN.length()) {
            unveraendert=false;
            //std::cerr<<"Länge unterschiedlich\n";
        }
        // Fingerabdruck entschluesseln und vergleichen
        for (int i=0; i<sig.length(); i++) {
            sig[i]=cur_pair.second.Entschluesseln(sig[i],true);
            if(sig[i]!=sigN[i]) {
                unveraendert=false;
                break;
            }
        }
        // PruefeNachricht
        PruefeNachricht(cur_pair.first,unveraendert);
        //std::cerr<<"\n _______________________\n"<<unveraendert<<"_______________________\n";
    }
    return 0;
}
