/****************************************************************
*  Name       : unit.h                                          *
*  Verwendung : Praktikumsumgebung (MAPRA) zum RSA-Verfahren    *
*  Autor      : A. Voss, IGPM, RWTH Aachen                      *
*  Datum      : SS 2001                                         *
*  Sprache    : C++11                                           *
****************************************************************/

#include <iostream>
#include <string>
#include <cassert>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <cstdlib>
#include <cstdint>

typedef uint64_t        uint64_t;
typedef int64_t         int64_t;
typedef std::set<uint64_t>  iSet;

//------------------------------------------------------------------------------
// diese Klasse kapselt die Ver- und Entschluesselung
//------------------------------------------------------------------------------
class RSA {
 public:
  RSA() : n(0), e(0), d(0) {}

  uint64_t	n;
  uint64_t	e; // public key
  //uint64_t	d;

 private:
  uint64_t	d; // private key
  uint64_t	p, q, phin; // phin entspricht phi(n) = (p-1)*(q-1)

 public:
  // TODO: output n,e,d
  friend std::ostream& operator<<(std::ostream& os, const RSA& pp);
  // TODO: input n,e
  friend std::istream& operator>>(std::istream& is, RSA& pp);

  // TODO: Verschluessel m mit public oder private key
  uint64_t Verschluesseln(const uint64_t& m, bool bPublic=true) const;
  // TODO: Entschluessel c mit public oder private key
  uint64_t Entschluesseln(const uint64_t& c, bool bPublic=false) const;

  // TODO: Generiere neue Schluessel, d.h. besetze n,e,d und p,q,phin
  void NeueSchluessel();

  // externe Pruefroutine, ist schon implementiert in unit
  friend void PruefeSchluessel(const RSA& rsa);

};

//------------------------------------------------------------------------------
// diese Klasse dient nur dazu, aus einem String einen
// gueltigen Fingerabdruck zu erzeugen;
// hier ist nichts zu tun
//------------------------------------------------------------------------------
class Signatur {
 private:
  enum { len = 16 };
  uint64_t m_block[len];

 public:
  uint64_t& operator[](unsigned int n) {
    if (n>=len) {
      std::cerr << "Fingerprint: Falscher Index " << n << std::endl;
    }
    return m_block[n];
  }

  const uint64_t& operator[](unsigned int n) const {
    if (n>=len) {
      std::cerr << "Fingerprint: Falscher Index " << n << std::endl;
    }
    return m_block[n];
  }

  static unsigned int length() { return len; }

  void set(const std::string& s);
};

//------------------------------------------------------------------------------
// diese Klasse enthaelt mit pMenge nichts weiter als die Menge der
// ersten k Primzahlen bis zur Groesse pMax
// der operator[](int n) gibt an, ob n in der pMenge enthalten ist, d.h.
// ob n < pMax prim ist
//------------------------------------------------------------------------------


class Prim {
 private:
  enum { pMax = 65536 };
  iSet pMenge;                     // iSet in oben definiert

 public:
  Prim() { init(); }

  void init();

  bool operator[](uint64_t n) const {
    if (n>=pMax) {
      std::cerr << "Falscher Index (" << n << ") in Prim::op[]" << std::endl;
      exit(1);
    }
    return (pMenge.find(n) != pMenge.end());
  }

  static uint64_t length() { return pMax; }
};



//------------------------------------------------------------------------------
// folgende Funktionen sind in unit implementiert, sie sind die Testumgebung
//------------------------------------------------------------------------------
void HoleNachricht(const std::string& Name, std::string& Text, Signatur& FP);

void PruefeNachricht(const std::string& Name, bool bValid);

void SchickeNachricht(const std::string& Text, const RSA& rsa, const Signatur& md5);

void PruefeSchluessel(const RSA& rsa);

//------------------------------------------------------------------------------
// folgende Funktionen dienen dem Test der eigenen Routinen
//------------------------------------------------------------------------------
void Pruefe_ggt( uint64_t (*f)(uint64_t,uint64_t) );
void Pruefe_xggt( uint64_t (*f)(uint64_t,uint64_t,int64_t&,int64_t&) );
void Pruefe_fastpow( uint64_t (*f)(uint64_t,uint64_t,uint64_t) );
