#include "unit.h"

using std::cout;
using std::cin;
using std::endl;

void show(unsigned int *&Feld, unsigned int laenge) // Gibt ein Array schön aus
{
    for (unsigned int i=0; i<laenge-1; i++)
        cout<<Feld[i]<<", ";
    cout<<Feld[laenge-1]<<endl;
}

void bubbleSort(unsigned int *&Feld, unsigned int laenge)
{
    bool fertig=true;//falls in einem durchlauf von j nicht getauscht, ist das Array sortiert
    for (unsigned int i=0,j=laenge; i<laenge; i++)//läuft von links
    {
        fertig=true;
        for (j=laenge-1; j>i; j--)//läuft von rechts bis i
        {
            if (Feld[j]<Feld[j-1])
            {
                tausche(Feld,j,j-1);//bubble
                fertig=false;
            }
        }
        if (fertig)
            return;
    }
}

void selectionSort(unsigned int *&Feld, unsigned int laenge)
{
    for (unsigned int i=0,j=laenge,pos=0; i<laenge; i++)//Stellen, an die richtiges Element gesetzt wird
    {
        pos=i;
        for (j=i+1; j<laenge; j++)//sucht das Minimum im Restarray
        {
            if (Feld[j]<Feld[pos])
            {
                pos=j;
            }
        }
        tausche(Feld,pos,i);
    }
}

void insertionSort(unsigned int *&Feld, unsigned int laenge)
{
    for(unsigned int i=1,h,j=0; i<laenge; i++) //trennt sortiertes und nicht sortiertes Array
    {
        for(j=0; j<i; j++)//durchläuft sortiertes Array
        {
            if(Feld[i]<Feld[j])//richtige Stelle gefunden
            {
                break;
            }
        }
        for (h=i; h>j; h--)//schiebt
            tausche(Feld,h,h-1);
    }
}

/**
*Returns the address of the left child
*/
inline unsigned int getLeft(unsigned int i, unsigned int laenge)
{
    unsigned int left=2*i+1;
    return left>=laenge?0:left;
}

/**
*Returns the address of the right child
*/
inline unsigned int getRight(unsigned int i, unsigned int laenge)
{
    unsigned int right=2*i+2;
    return right>=laenge?0:right;
}

/**
*Returns the address of the maximum child
*/
inline unsigned int getMax(unsigned int *&Feld, unsigned int i, unsigned int laenge)
{
    unsigned int left=getLeft(i,laenge), right=getRight(i,laenge);
    return Feld[left]>Feld[right]?left:right;
}

/**
* Bringt das Array in eine Heap-artige Reihenfolge
*/
void heapify(unsigned int *&Feld, unsigned int i, unsigned int laenge)
{
    unsigned int m=getMax(Feld,i,laenge); //Adresse das maximalen Kindes
    if (m==0) //Kein Kindknoten, fertig
    {
        return;
    }
    if (Feld[i]<Feld[m]) //Falls Kindknoten größer tausche mit diesem und repariere den unteren Heap
    {
        tausche(Feld,i,m);
        heapify(Feld,m,laenge);
    }
}

void heapSort(unsigned int *&Feld, unsigned int laenge)
{
    for (unsigned int j=laenge-1; j>0; j--) //Baue den Heap auf, Element 0  wird in der nächsten Schleife sowieso gemacht
    {
        heapify(Feld,j,laenge);
    }
    for (unsigned int i=0; i<laenge; i++)
    {
        heapify(Feld,0,laenge-i); //Korrigiere die Heapstruktur an der Wurzel, beachte aber nur die unsortierten Einträge
        tausche(Feld,laenge-i-1,0); //Tausche das lokal größte Element mit dem lokal letzten
    }
}

unsigned int med3(unsigned int *&Feld, unsigned int start, unsigned int stop) {
    unsigned int mid=(start+stop)/2;
    if (Feld[start]>Feld[stop])
    {
        if (Feld[start]>Feld[mid])
        {
            if (Feld[mid]>Feld[stop])
                return mid;
            else return stop;
        }
        else
            return start;
    }
    else
    {
        if (Feld[start]<=Feld[mid])
        {
            if (Feld[mid]<=Feld[stop])
                return mid;
            else return stop;
        }
        else
            return start;
    }
}
/**
*	@var stop Die Adresse des letzten Elements
*/
void quickSort(unsigned int *&Feld, unsigned int stop, bool median3=false, unsigned int start=0) {
    if (stop==0||start==stop) //doofe Grenzfaelle, ersteres duerfte nicht passieren
        return;
    unsigned int pivot=stop, i=start, j=stop-1;
    if (median3) {
        tausche(Feld,pivot,med3(Feld,start,stop));
    } //ab jetzt ganz normal quicksort, nur Median3 mitschleppen

    while(i<j) {
        for(; i<j&&Feld[i]<=Feld[pivot]; i++); //Suche zwei Elemente die auf der falschen Seite des Pivots stehen,
        for(; i<j&&Feld[j]>=Feld[pivot]; j--); //ignoriere die mit gleicher Wertigkeit
        if (i<j)
            tausche(Feld,i,j);
    }
    if (Feld[i]<Feld[pivot]) { //Altes Pivot steht jetzt bei i+1
        tausche(Feld,i+1,pivot);
        if (i>start) //nicht links raus laufen
            quickSort(Feld,i, median3, start);
        if (i+2<stop) //nicht rechts raus laufen
            quickSort(Feld, stop, median3, i+2);
    }
    else { //Altes Pivot steht jetzt bei i
        tausche(Feld,i,pivot);
        if (i>start+1)
            quickSort(Feld,i-1, median3, start);
        if (i+1<stop)
            quickSort(Feld, stop, median3, i+1);
    }
}

void merge(unsigned int *&Feld, unsigned int laenge, unsigned int *&feld1, unsigned int laenge1, unsigned int *&feld2, unsigned int laenge2) {
    unsigned int zeiger1=0, zeiger2=0;
    for (unsigned int i=0; i<laenge; i++) { //feld1 und feld2 sind sortiert, füge nun richtig zusammen
        if (zeiger1<laenge1) { //feld1 nicht "leer"
            if (zeiger2<laenge2) { //feld2 nicht "leer"
                if (feld1[zeiger1]<feld2[zeiger2]) //Falls beide nicht leer füge das Minimum in Feld ein
                    Feld[i]=feld1[zeiger1++]; //und erhöhe den Zähler in dem Array woher das Minimum kommt
                else
                    Feld[i]=feld2[zeiger2++];
            }
            else
                Feld[i]=feld1[zeiger1++];	//falls eins leer ist nimm das andere
        }
        else if (zeiger2<laenge2)
            Feld[i]=feld2[zeiger2++];
    }
}

void mergeSort(unsigned int *&Feld, unsigned int laenge) {
    if (laenge<=1) //Grenzfall
        return;
    unsigned int l1=laenge/2,l2=laenge-l1,i; //Teile mehr oder weniger gleichmäßig auf
    unsigned int *feld1=new unsigned int[l1],*feld2=new unsigned int[l2];
    for (i=0; i<l1; i++) {	//Kopiere in die Teilarrays
        feld1[i]=Feld[i];
    }
    for (; i<laenge; i++) {
        feld2[i-l1]=Feld[i];
    }
    mergeSort(feld1,l1); //Sortiere rekursiv
    mergeSort(feld2,l2);
    merge(Feld,laenge,feld1,l1,feld2,l2); //merge
    delete[] feld1;	//räume auf
    delete[] feld2;
}

int main() {
    unsigned int *Feld;
    unsigned int Beispiel,laenge,alg;
    cout<<"Welches Beispiel? (muss >0 und <="<<AnzahlBeispiele<<" sein!)\n";
    cin>>Beispiel;
    cout<<"Wie lang soll das Beispiel sein?\n";
    cin>>laenge;
    cout<<"Welcher Algorithmus? \n (1) Bubble-Sort \n (2) Selection-Sort \n (3) Insertion-Sort \n (4) Heapsort \n (5) Quicksort schlecht \n (6) Quicksort gut \n (7) Mergesort\n";
    cin>>alg;
    start(Beispiel, laenge, Feld);
    switch (alg)
    {
    case 1 :
        bubbleSort(Feld,laenge);
        break;
    case 2 :
        selectionSort(Feld,laenge);
        break;
    case 3 :
        insertionSort(Feld,laenge);
        break;
    case 4 :
        heapSort(Feld,laenge);
        break;
    case 5 :
        quickSort(Feld,laenge-1);
        break;
    case 6 :
        quickSort(Feld,laenge-1,true);
        break;
    case 7 :
        mergeSort(Feld,laenge);
        break;
    default :
        cout<<"Gibt es nicht!\n Auf Wiedersehen\n";
    }
    ergebnis(Feld);
    return 0;
}

