/***********************************************************
*  Name       : meina1.cpp                                 *
*  Verwendung : Loesung der ersten Praktikumsaufgabe,      *
*               Nullstellen eines quadratischen Polynoms   *
*  Autor      : Anna Limbach, Daniel Steenebrügge          *
*  Datum      :                                            *
*  Sprache    : C++                                        *
***********************************************************/

// Einbinden der Praktikums-Umgebung. Falls Sie die Ein-/Ausgabe zuerst
// nicht ueber die Praktikumsumgebung machen wollen, sollten Sie auch noch
// #include <iostream> einbinden.

#include "unit.h"

#include <cmath>

// ===== Hauptprogramm =====

int main()
{
    double a,b,c,rad,x,x2;
    bool P;
    int i;
    for(int n=1; n<=AnzahlBeispiele; n++) { //Loop über alle Beispiele
        Start(n,a,b,c); //Besorge die Koeffizienten
        if(a==0) { //Nicht wirklich quadratisch
            if(b==0) { //Kein linearer Anteil
                if(c==0) { //Null-Funktion
                    Ergebnis(Unendlich);
                }
                else //y=c!=0
                {
                    Ergebnis(0);
                }
            }
            else //affine Funktion
            {
                Ergebnis(1,false,-c/b);
            }
        }
        else
        {
            double p=b/a, q=c/a; //berechne p und q
            if(p>0) {
                i=-1; // x2 zuerst berechnen
            }
            else
            {
                i=1; //x1 zuerst berechnen
            }
            if(-i*p>sqrt(DBL_MAX)) { //Overflow verhindern
                P=true; //Rechte Variante
                rad=1.0/4.0-(q/p)/p;
            }
            else
            {
                P=false; //Linke Variante
                rad=(p/2.0)*(p/2.0)-q;
            }
            if(rad>=0) { //Reell
                if(rad==0) { //Beide Nullstellen fallen zusammen
                    Ergebnis(1,false,p/2.0);
                }
                else
                {
                    if(P) { //Rechte Variante
                        x=-p*sqrt(rad)-p/2.0;
                    }
                    else
                    {   //Linke Variante
                        x=i*sqrt(rad)-p/2.0;
                    }
                    if(x==0) { //x==0 macht Satz von Vieta doof, aber trotzdem stabil berechenbar
                        Ergebnis(2,false,b/a,0);
                    }
                    else //Berechne mit Satz von Vieta
                    {
                        x2=q/x;
                        Ergebnis(2,false,x,x2);
                    }
                }
            }
            else //Komplexer Fall
            {
                Ergebnis(2, true, -p/2.0, P ? -i*p*sqrt(-rad) : sqrt(-rad));
            }
        }
    }
    return 0;
}
