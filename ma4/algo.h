/***********************************************************
*  Name       : algo.h                                     *
*  Verwendung : Verschiedene String Matching Algorithmen   *
*  Autor      :                                            *
*  Datum      :                                            *
*  Sprache    : C++                                        *
***********************************************************/
#include <map>

/**
* \brief Basis-Klasse die einen String Matching Algorithmus repraesentiert. Wichtig: Die Einzelnen Algorithmen werden von dieser Klasse abgeleitet.
*/
class SMAP
{
public:
    /**
    * \brief Fuert die Suche nach einem Pattern im Text durch.
    * \param s Der Text in dem gesucht werden soll.
    * \param p Das Pattern nach dem gesucht werden soll.
    * \param result Vektor in dem die Fundstellen gespeichert werden sollen.
    */
    virtual void find_single(const string &s, const string &p, vector<int> &result) = 0;
    /**
    * \brief Fuert die Suche nach mehreren Pattern im Text durch.
    * \param s Der Text in dem gesucht werden soll.
    * \param p Die Pattern nach denen gesucht werden soll.
    * \param result Vektor in dem die Fundstellen gespeichert werden sollen.
    */
    virtual void find_multiple(const string &s, const vector<string> &p, vector<vector<int> > &result) = 0;
protected:
    string name;
    SMAP():name("SMAP") {}
    SMAP(string s):name(s) {}
};

class Simple : public SMAP
{
public:
    void find_multiple(const string &s, const vector<string> &p, vector<vector<int> > &result);
protected:
    Simple(string s):SMAP(s) {}
};

class Naiv : public Simple
{
public:
    Naiv():Simple("Naiv") {}
    void find_single(const string &s, const string &p, vector<int> &result);
};

class KnuthMorrisPratt : public Simple
{
private:
    vector<int> f;

public:
    KnuthMorrisPratt():Simple("KnuthMorrisPratt") {}
    void find_single(const string &s, const string &p, vector<int> &result);
};

class BoyerMoore : public Simple
{
private:
    /**
    *\brief erstes Vorkommen von rechts, gezählt von links
    */
    map<char,int> erstes_Vorkommen;
    /**
    *\brief enthält die zuerst die Position, dann die Länge
    */
    vector<pair<int,int>> laengstes_Suffix;
    /**
    *\brief initialisiert die ersten Vorkommen
    *\param p untersuchtes pattern
    */
    void initialisiere_map(const string& p);
    /**
    *\brief initialisiert laengstes_Suffix mit den Positionen und Längen des längsten rechtesten Suffix
    *\param p untersuchtes pattern
    */
    void initialisiere_laengstes_Suffix(const string& p);
    /**
    * \brief gibt die am weitesten rechts gelegene Stelle im Pattern p zurück, an der der character a steht
    *\param p untersuchtes pattern
    *\param a Buchstabe, dessen Position in p gesucht wird
    */
    int finde_letztes_Vorkommen(char a, const string& p);
    /**
    * \brief berechnet die Größe der optimalen Verschiebung
    *\param anfangPattern Position, an der sich das erste Zeichen des Patterns p relativ zum string s befindet
    *\param posP Stelle, an der der Vergleich fehlgeschlagen ist
    *\param linke_Grenze markiert die Stelle, bis zu der nach dem Verschieben nicht mehr verglichen werden muss
    *\param p betrachtetes Pattern
    */
    void verschiebung(int& anfangPattern, int& posP, int& linke_Grenze, char buchstabe, const string& p);

public:
    /**
    * \brief Standard-Konstruktor, gibt Identifikationsmöglichkeit
    */
    BoyerMoore():Simple("BoyerMoore") {}
    /**
    * \brief Findet nach dem BoyerMoore-Algorithmus alle Vorkommen von p in s und speichert ihre Anfangspositionen in result
    * \param s Der String in dem p gesucht werden soll
    * \param p Das Pattern das gesucht werden soll
    * \param result Der Vektor der die Anfangspositionen aller Vorkommen enthält
    */
    void find_single(const string &s, const string &p, vector<int> &result);
};

template<class H>
class RabinKarp : public SMAP
{
public:
    /**
    * \brief Standard-Konstruktor, gibt Identifikationsmöglichkeit
    */
    RabinKarp():SMAP("RabinKarp") {
        name.append(H::name);
    }
    /**
    * \brief Findet durch Hashing alle Vorkommen des Patterns p im String s und speichert die Anfangspositionen in result
    * \param s Der String in dem gesucht wird
    * \param p Das Pattern nach dem gesucht wird
    * \param result Der Vektor in dem alle Anfangspositionen gespeichert werden
    */
    void find_single(const string &s, const string &p, vector<int> &result);
    /**
    * \brief Findet durch Hashing alle Vorkommen aller Patterns in p im String s und speichert die Anfangspositionen in result
    * \param s Der String in dem gesucht wird
    * \param Vektor der Pattern nach denen gesucht wird
    * \param result Der Vektor von Vektoren in denen die Anfangspositionen zum entsprechenden Pattern gespeichert werden
    */
    void find_multiple(const string &s, const vector<string> &p, vector<vector<int> > &result);
};


void Simple::find_multiple(const string &s, const vector<string> &p, vector<vector<int> > &result)
{
    start_multiple();

    // Aufgabe
    int ps=p.size();
    result.resize(ps);
    for (int i=0; i<ps; i++) {
        find_single(s,p[i],result[i]);
    }

    finish_multiple(name, s, p, result);
}

void Naiv::find_single(const string &s, const string &p, vector<int> &result)
{
    start();
    result.resize(0);
    int ls=s.size(), lp=p.size();
    for (int i=0,j; i<ls-lp+1; i++) {
        for (j=0; j<lp; j++) {
            if(!compare(s[i+j],p[j])) {
                break;
            } else if (j==lp-1) {
                result.push_back(i);
            }
        }
    }
    // Aufgabe

    finish(name);
}

void KnuthMorrisPratt::find_single(const string &s, const string &p, vector<int> &result)
{
    start();

    // Aufgabe

    //Initialisiere Variablen:
    int ls=s.size(), lp=p.size(), i, l, j, lokal_laengstes;
    result.resize(0);
    //Initialisiere f
    f.resize(lp+1);
    f[0]=0;
    lokal_laengstes=0;
    for (i=1; i<=lp; i++) { //i läuft von vorne, l von "hinten", wird aber trotzdem erhöht
        for (l=lokal_laengstes+1; l<i; l++) {
            for (j=0; j<l; j++) {
                if (!compare(p[j],p[i-l+j])) {
                    break;
                } else if (j==l-1) {
                    lokal_laengstes=l;
                }
            }
        }
        f[i]=lokal_laengstes;
    }

    //Laufe durchs Array
    l=0;
    i=0;
    while(l<ls-lp+1) { //l läuft durch den ganzen String
        while(i<lp) {
            if (!compare(s[l+i], p[i])) {
                break;
            } else if (i==lp-1) {
                result.push_back(l);
            }
            i++;
        }
        if (i==0) {
            l++;
        } else {
            l=l+i-f[i];
            i=f[i];
        }
    }

    finish(name);
}

void BoyerMoore::initialisiere_map(const string& p) { //erstes_Vorkommen ordnet jedem Buchstaben die rechteste position zum, in der es im 								Pattern p steht
    int lp=p.size();
    erstes_Vorkommen.clear();
    for(int i=0; i<lp; i++) {
        if(erstes_Vorkommen.find(p[lp-i-1])==erstes_Vorkommen.end()) {
            erstes_Vorkommen[p[lp-i-1]]=lp-i-1;
        }
    }
}

int BoyerMoore::finde_letztes_Vorkommen(char a, const string& p) {
    auto zeiger=erstes_Vorkommen.find(a);
    if(zeiger==erstes_Vorkommen.end()) {
        return -1;
    } else {
        return (*zeiger).second;
    }
}

void BoyerMoore::initialisiere_laengstes_Suffix(const string& p) {
    int lEndstk, lmaxSuffix, lSuffix, pSuffix, pgrSuffix, lp=p.size(), testeSuffix;
    bool gefunden,linker_Rand=false;
    laengstes_Suffix.assign(lp+1,pair<int,int>(lp-1,0));
    lmaxSuffix=0;
    pgrSuffix=lp-1;
    for (lEndstk=1; lEndstk<lp+1; lEndstk++) {
        //Laufe von links durch
        for (lSuffix=lmaxSuffix+1; lSuffix<=lEndstk && !linker_Rand; lSuffix++) {
            gefunden=false;
            if (pgrSuffix>0 && compare(p[pgrSuffix-1],p[lp-lmaxSuffix-1])) { //Ganz links steht schon gueltiges Suffix
                lmaxSuffix++;
                pgrSuffix=pSuffix-1;
                continue;
            }
            for (pSuffix=pgrSuffix; pSuffix>=0 && !gefunden; pSuffix--) { //Wenn hier am linken Rand angekommen kann es kein besseres Suffix mehr geben
                testeSuffix=0;
                while(testeSuffix<=lSuffix && compare(p[pSuffix+testeSuffix],p[lp-lSuffix/*-1*/+testeSuffix])) {
                    if (testeSuffix==lSuffix-1 && !pSuffix+testeSuffix==lp-1) {
                        lmaxSuffix=lSuffix;
                        pgrSuffix=pSuffix;
                        gefunden=true;
                    }
                    testeSuffix++;
                }
            }
            if (!gefunden && pSuffix==0) {
                linker_Rand=true;
            }
        }
        laengstes_Suffix[lEndstk]=pair<int,int>(pgrSuffix,lmaxSuffix);
    }
}
void BoyerMoore::verschiebung(int& anfangPattern, int& posP, int& linke_Grenze, char buchstabe, const string& p) {
    int vnb=finde_letztes_Vorkommen(buchstabe, p),lp=p.size(),lEndstk=lp-posP-1;
    pair<int,int> vns=laengstes_Suffix[lEndstk];
    int vnsp=vns.first, vnsl=vns.second, verschiebung_nach_Pattern=lp-vnsl-vnsp, verschiebung_nach_Buchstabe=posP-vnb;
    if (verschiebung_nach_Pattern>verschiebung_nach_Buchstabe && verschiebung_nach_Pattern>0) {
        anfangPattern+=verschiebung_nach_Pattern;
        posP=lp-1;
        linke_Grenze=vnsp+vnsl+1;
    } else if (verschiebung_nach_Buchstabe>0) {
        linke_Grenze=0;
        anfangPattern+=verschiebung_nach_Buchstabe;
        posP=lp-1;
    } else {
        anfangPattern++;
        posP=lp-1;
        linke_Grenze=0;
    }

}

void BoyerMoore::find_single(const string &s, const string &p, vector<int> &result)
{
    start();

    // Aufgabe
    result.resize(0);
    initialisiere_map(p);
    initialisiere_laengstes_Suffix(p);
    int anfangPattern=0, lS=s.size(), lP=p.size(), posP=lP-1, linke_Grenze=0;//anfang_pattern ist die Position im string, an der der Anfang des Patterns steht,
    //posP ist die position im Pattern, an der gerade verglichen wird
    while (anfangPattern<lS-lP+1) {
        for( ; posP>=linke_Grenze; posP--) {
            if (!compare(s[anfangPattern+posP],p[posP]))
                break;
            if (posP==linke_Grenze) {
                result.push_back(anfangPattern);
            }
        }
        verschiebung(anfangPattern, posP, linke_Grenze, s[anfangPattern+posP], p);
    }
    finish(name);
}

template<class H>
void RabinKarp<H>::find_single(const string &s, const string &p, vector<int> &result)
{
    start();

    vector<string> pp;
    pp.push_back(p);
    vector<vector<int> > rresult;

    find_multiple(s,pp,rresult);
    result = rresult[0];

    finish(name);
}

template<class H>
void RabinKarp<H>::find_multiple(const string &s, const vector<string> &p, vector<vector<int> > &result)
{
    start_multiple();
    start();

    // Aufgabe
    //finde kürzestes Pattern
    int min_size=p[0].size(), cur_size, lS=s.size();
    for (string pi : p) {
        cur_size=pi.size();
        if (cur_size<min_size) {
            min_size=cur_size;
        }
    }
    //Lege die Hash-Tabelle an
    H hash(min_size);
    //mache Patterns gleich lang und speichere die Hashes
    vector<string> p_eq(p);
    for (int i=p_eq.size()-1; i>=0; i--) {
        p_eq[i]=p_eq[i].substr(0, min_size);
        hash.set(p_eq[i]);
        hash.save(i);
    }
    //lege Vektor mit möglichen Ergebnissen an
    vector<vector<int>> possible_result(p.size(),vector<int>());
    hash.set(s.substr(0,min_size)); //initialisiere hash
    HashList *hList=hash.find(); //Deklariere eine HashList zur weiteren Verwendung
    while (!compareNULL(hList)) { //Prüfe für 0, Falls mehrere Patterns den gleichen Hash haben nimm alle
        possible_result[(*hList).value].push_back(0);
        hList=(*hList).next;
    }
    for (int pos=0; pos<lS-min_size; pos++) { //Durchsuche String nach potentiellen Matches
        hash.roll(s[pos],s[pos+min_size]); //lS-1>=pos+min_size
        hList=hash.find();
        while (!compareNULL(hList)) { //Falls mehrere Patterns den gleichen Hash haben nimm alle
            possible_result[(*hList).value].push_back(pos+1);
            hList=(*hList).next;
        }
    }
    //Gehe die potentiellen Matches durch und prüfe, ob es sich um echte Matches handelt
    result.resize(p.size());
    for (int pN=p.size()-1; pN>=0; pN--) { //Gehe die Patterns durch
        for (int pos : possible_result[pN]) { //Gehe die potentiellen Stellen durch
            for (int i=p[pN].size()-1; i>=0; i--) { //Prüfe stellenweise
                if (!compare(p[pN][i], s[pos+i]))
                    break;
                if (i==0)
                    result[pN].push_back(pos);
            }
        }
    }

    finish(name);
    finish_multiple(name, s,p, result);
}

