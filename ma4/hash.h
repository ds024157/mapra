/***********************************************************
*  Name       : hash.h                                     *
*  Verwendung : Verschiedene Hash-Funktionen               *
*  Autor      :                                            *
*  Datum      :                                            *
*  Sprache    : C++                                        *
***********************************************************/

#include <cmath>

using namespace std;

/**
* \brief Klasse die das Arbeiten mit Hash-Funktionen beinhaltet. Sie beinhaltet den Hash-Wert, eine Hash-Tabelle und fuehrt auch die Suche nach einem Hash-Wert durch.
* \param val_hash Der aktuell betrachtete Hash-Wert
* \param len Laenge der Woerter von denen der Hash-Wert berechnet werden kann
* \param table Die Hash-Tabelle. Speichert zu jedem Hash-Wert eine Liste vom Typ HashList. $table[i]$ soll die Liste aller Pattern sein, die den Hash-Wert $i$ haben.
* \param name Name der Hash-Funktion.
*/
class RollingHash
{
public:
    /**
    * \brief Fuehrt den roll-Algorithmus durch. Der alte Hash-Wert ist val_hash und das Ergebnis wird ebenfalls in val_hash gespeichert.
    * \param del Buchstabe der aus dem Hash-Wert geloescht werden soll
    * \param add Buchstabe der zu dem Hash-Wert hinzugefuegt werden soll
    */
    virtual void roll(const char &del, const char &add) = 0;
    /**
    * \brief Berechnet den Hash-Wert fuer einen gegebenen String.
    * \param s String dessen Hash-Wert berechnet werden soll
    */
    virtual void set(const string &s) = 0;
    /**
    * \brief Setzt val_hash zurueck auf 0.
    */
    void del();
    /**
    * \brief Speichert in der Hash-Tabelle an der Stelle val_hash den Wert v ab
    * \param v Wert der abgespeichert werden soll
    */
    void save(int val);
    virtual void init(int m) = 0;
    /**
    * \brief Gibt die HashList zurueck, die in der Hash-Tabelle an der Stelle val_hash gespeichert ist.
    */
    HashList* find();
    /**
    * \brief Gibt den aktuellen Hash-Wert zurueck, also val_hash.
    */
    int value();

protected:
    int val_hash;
    int len;
    vector<HashList*> table;
    static string name;

};


void RollingHash::save(int v)
{
    if(table[val_hash]!=NULL)
        table[val_hash]->add(v);
    else
        table[val_hash] = new HashList(v);
}

HashList* RollingHash::find()
{
    return table[val_hash];
}

void RollingHash::del()
{
    val_hash=0;
}

int RollingHash::value()
{
    return val_hash;
}

/**
* \brief Klasse die das Arbeiten mit Hash-Funktionen beinhaltet. Sie beinhaltet den Hash-Wert, eine Hash-Tabelle und fuehrt auch die Suche nach einem Hash-Wert durch.
* \param val_hash Der aktuell betrachtete Hash-Wert
* \param len Laenge der Woerter von denen der Hash-Wert berechnet werden kann
* \param table Die Hash-Tabelle. Speichert zu jedem Hash-Wert eine Liste vom Typ HashList. $table[i]$ soll die Liste aller Pattern sein, die den Hash-Wert $i$ haben.
* \param name Name der Hash-Funktion.
*/
class HashSimple : public RollingHash
{
public:
    /**
    * \brief (Standart-)Konstruktor, setzt die Länge
    * \param m Die Länge des Patterns das gehasht werden soll
    */
    HashSimple(int m=0);
    /**
    * \brief Konstruktor, setzt die Länge und setzt value auf den Hash-Wert
    * \param m Die Länge des Patterns das gehasht werden soll
    * \param s Ein zu hashender String
    */
    HashSimple(int m, const string &s);
    /**
    * \brief Ändert den Wert des Hashs (value) auf den Wert, der herauskäme, wenn das Wort um eins nach rechts verschoben wird
    * \param del Der char der vorher der erste Buchstabe des gehashten Worts war
    * \param add Der char der der neue letzte Buchstabe des gehashten Worts sein soll
    */
    void roll(const char &del, const char &add);
    /**
    * \brief Hasht das Wort s. Der Wert kann mit value() ausgelesen werden.
    * \param s Das zu hashende Wort
    */
    void set(const string &s);
    /**
    * \brief Initialisiert die Hash-Funktion, so dass ein Wort der Länge m gehasht werden kann
    * \param m Die Länge der zu hashende Wörter
    */
    void init(int m);
    static string name;
};
string HashSimple::name = "HashSimple";

void HashSimple::init(int m)
{
    val_hash = 0;
    len = m;
    table = vector<HashList*>(m*UCHAR_MAX);
}

void HashSimple::roll(const char &del, const char& add)
{
    // Aufgabe
    val_hash+=(int)(abs(add)-abs(del));
}
HashSimple::HashSimple(int m)
{
    init(m);
}

HashSimple::HashSimple(int m, const string &s)
{
    init(m);
    set(s);
}

void HashSimple::set(const string &s)
{
    // Aufgabe
    //init(s.size()); //braucht man nicht, wird im Konstruktor gemacht
    val_hash=0;
    for (char c :s) {
        val_hash+=(int)abs(c);
    }
}

/**
* \brief Klasse die das Arbeiten mit Hash-Funktionen beinhaltet. Sie beinhaltet den Hash-Wert, eine Hash-Tabelle und fuehrt auch die Suche nach einem Hash-Wert durch.
* \param val_hash Der aktuell betrachtete Hash-Wert
* \param len Laenge der Woerter von denen der Hash-Wert berechnet werden kann
* \param table Die Hash-Tabelle. Speichert zu jedem Hash-Wert eine Liste vom Typ HashList. $table[i]$ soll die Liste aller Pattern sein, die den Hash-Wert $i$ haben.
* \param name Name der Hash-Funktion.
*/
class HashBetter : public RollingHash
{
public:
    /**
    * \brief (Standart-)Konstruktor, setzt die Länge
    * \param m Die Länge des Patterns das gehasht werden soll
    */
    HashBetter(int m=0);
    /**
    * \brief Konstruktor, setzt die Länge und setzt value auf den Hash-Wert
    * \param m Die Länge des Patterns das gehasht werden soll
    * \param s Ein zu hashender String
    */
    HashBetter(int m, const string &s);
    /**
    * \brief Ändert den Wert des Hashs (value) auf den Wert, der herauskäme, wenn das Wort um eins nach rechts verschoben wird
    * \param del Der char der vorher der erste Buchstabe des gehashten Worts war
    * \param add Der char der der neue letzte Buchstabe des gehashten Worts sein soll
    */
    void roll(const char &del, const char &add);
    /**
    * \brief Hasht das Wort s. Der Wert kann mit value() ausgelesen werden.
    * \param s Das zu hashende Wort
    */
    void set(const string &s);
    /**
    * \brief Initialisiert die Hash-Funktion, so dass ein Wort der Länge m gehasht werden kann
    * \param m Die Länge der zu hashende Wörter
    */
    void init(int m);
    static string name;
private:
    int pri;
    int mod;
    int phm;
    int q;
};

string HashBetter::name = "HashBetter";

void HashBetter::init(int m)
{
    val_hash = 0;
    len = m;
    pri = 256;
    mod = 103;

    table = vector<HashList*>(mod);

    q=1;
    for (int i=0; i<len; i++) {
        q=(pri*q) % mod;
    }
}

void HashBetter::roll(const char &del, const char& add)
{
    // Aufgabe
    val_hash=(
                 val_hash*pri //h_{old} * a
                 +(int)abs(add) //char(w_{k+1})
                 +mod //z
                 -( //-(q*char(w_1) mod z
                     (
                         q
                         *((int)abs(del)) //char(w_1)
                     ) % mod //mod z
                 )
             )% mod //mod z
             ;

}
HashBetter::HashBetter(int m)
{
    init(m);
}

HashBetter::HashBetter(int m, const string &s)
{
    init(m);
    set(s);
}

void HashBetter::set(const string &s)
{
    // Aufgabe
    val_hash=0;
    for (int i=0; i<len; i++) {
        val_hash*=pri;
        val_hash+=(int)abs(s[i]);
        val_hash%=mod;
    }
}


