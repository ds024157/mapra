/***********************************************************
*  Name       : string_matching.cpp                        *
*  Verwendung : Rahmenprogramm String Matching Aufgabe     *
*  Autor      :                                            *
*  Datum      :                                            *
*  Sprache    : C++                                        *
***********************************************************/

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <climits>
#include <ctime>
#include <string>


#include "util.h"
#include "hash.h"
#include "algo.h"

using namespace std;

/**
* \brief Hauptroutine des Programms. Es werden alle Beispiele oder einzelene Beispiele geladen und dann die entsprechenden Algorithmen durchgefuehrt.
*/
int main()
{
    cout << "*************************************************************************" << endl;
    cout << "*Willkommen zum String Matching Algorithmen Programm - oder kurz:  SMAP!*" << endl;
    cout << "*************************************************************************" << endl << endl;
    int exnr = -1;

    cout << "Alle Beispiele laufen lassen? (1=Ja, 0=Nein) ";
    cin >> exnr;
    cout << endl;

    int ex_start = 0;
    int ex_end=NUM_EXAMPLES;

    if(exnr==0)
    {
        cout << "Welches Beispiel? (0.." << (NUM_EXAMPLES-1) << ")";
        cin >> exnr;
        ex_start = exnr;
        ex_end = exnr+1;
    }

    for(int i=ex_start; i<ex_end; ++i)
    {
        string T;
        vector<string> P;
        example(i,T,P);

        vector<vector<int> > result;

        Naiv naiv;
        result.clear();
        naiv.find_multiple(T,P,result);

        KnuthMorrisPratt kmp;
        result.clear();
        kmp.find_multiple(T,P,result);

        BoyerMoore bm;
        result.clear();
        bm.find_multiple(T,P,result);

        RabinKarp<HashSimple> rk1;
        result.clear();
        rk1.find_multiple(T,P,result);

        RabinKarp<HashBetter> rk2;
        result.clear();
        rk2.find_multiple(T,P,result);
    }

    cout << endl << endl;
    return 0;

}
