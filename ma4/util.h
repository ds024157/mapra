/** \file util.h */
#ifndef _UTIL_H
#define _UTIL_H

#define RESET   "\033[0m"
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */

#define NUM_EXAMPLES 3

#define METHOD_NAIV 0
#define METHOD_KMP 1
#define METHOD_BM 2
#define METHOD_RK 3
#define SHOW_OUTPUT false
#define SHOW_OUTPUT_MUL true


using namespace std;

/**
* \brief Klasse zum Speichern von Daten in einer Hash-Tabelle. Ein Objekt der Klasse repraesentiert eine Liste von Elementen, wobei next immer auf das naechste Objekt der Liste zeigt.
* \param value Wert der an dieser Stelle gespeichert werden soll
* \param next Naechstes Element der Liste
*/
class HashList
{
public:
    /**
    * \brief Fuegt den Wert v in die Liste ein. Das bedeutet am Ende der Liste wird ein neues Element angefuegt, das den Wert v als value beinhaltet.
    */
    void add(int v);
    HashList(int v);
    HashList();
public:
    int value;
    HashList *next;
};



/**
* \defgroup start-finish Initialisierung / Ergebnisvergleich
* \brief Funktionen um die Praktikumsumgebung zu initialisieren und Ergebnisse zu vergleichen.
* @{
*/
void start();
void start_multiple();
void finish(const string &s);
void finish_multiple(const string &s, const string &text, const vector<string> &p, const vector<vector<int> > &result);
/** @} */

/**
* \defgroup compare Vergleichsfunktionen
* \brief Funktionen zum Vergleichen von Werten. Hierbei wird jeder Vergleich gezaehlt und spaeter die Anzahl an Vergleichen ausgegeben.
* @{
*/
bool compare(bool a, bool b);
bool compare(int a, unsigned long b);
bool compare(char a, char b);
bool compareNULL(HashList* a);
/** @} */

/**
* \defgroup show_result Ergebnisausgabe
* \brief Funktionen zur Ausgabe von Ergebnissen.
* \param l Funktion gibt l Zeichen vor und nach dem Fund aus.
* \param s Der Text in dem gesucht wurde.
* \param p Pattern nach denen gesucht wurde.
* \param result Die Stellen an denen die Pattern gefunden wurden.
* @{
*/
void show_result_single(string &s, string &p, vector<int> &result, int l);
void show_result_multiple(const string &s, const vector<string> &p, const vector<vector<int> > &result, int l);
/** @} */

/**
* \defgroup compare Beispiele
* \brief Funktion zum Laden eines Beispiels.
* \param nr Nummer des Beispiels.
* \param T  Text der aus dem Beispiel geladen wird.
* \param P  Alle Pattern nach denen im Beispiel gesucht werden soll.
* @{
*/
void example(int nr, string &T, vector<string> &P);
/** @} */


#endif
